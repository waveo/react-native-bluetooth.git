require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-bluetooth"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                  bluetooth
                   DESC
  s.homepage     = "https://gitee.com/waveo/react-native-bluetooth"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Ming Chen" => "mingchen@waveo.com" }
  s.platforms    = { :ios => "9.0", :tvos => "10.0" }
  s.source       = { :git => "https://gitee.com/waveo/react-native-bluetooth.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.frameworks = 'CoreBluetooth'

  s.dependency "React"
	
  # s.dependency "..."
end

