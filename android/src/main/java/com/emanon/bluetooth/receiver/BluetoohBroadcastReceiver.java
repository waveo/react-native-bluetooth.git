package com.emanon.bluetooth.receiver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.emanon.bluetooth.listener.OnBluetoothStateChangedListener;


public class BluetoohBroadcastReceiver extends BroadcastReceiver {
  private OnBluetoothStateChangedListener bluetoothStateChangedListener;

  public BluetoohBroadcastReceiver() { super(); }

  public BluetoohBroadcastReceiver(
      OnBluetoothStateChangedListener bluetoothStateChangedListener) {
    super();

    this.bluetoothStateChangedListener = bluetoothStateChangedListener;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();

    if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
      if (bluetoothStateChangedListener != null) {
        bluetoothStateChangedListener.onBluetoothStateChanged();
      }
    }
  }
};
