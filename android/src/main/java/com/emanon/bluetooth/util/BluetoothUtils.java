package com.emanon.bluetooth.util;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

import java.util.List;

public class BluetoothUtils {

    public static WritableMap characteristic2Data(BluetoothGattCharacteristic characteristic) {

        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("uuid", characteristic.getUuid().toString());
        writableMap.putString("value", characteristic.getStringValue(0));
        writableMap.putArray("descriptors", BluetoothUtils.descriptors2Data(characteristic.getDescriptors()));
        return writableMap;
    }

    public static WritableArray characteristics2Data(List<BluetoothGattCharacteristic> characteristics) {

        WritableArray writableArray = Arguments.createArray();
        for (BluetoothGattCharacteristic characteristic : characteristics) {
            writableArray.pushMap(BluetoothUtils.characteristic2Data(characteristic));
        }
        return writableArray;
    }

    public static WritableMap service2Data(BluetoothGattService service) {

        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("uuid", service.getUuid().toString());
        writableMap.putArray("characteristics", BluetoothUtils.characteristics2Data(service.getCharacteristics()));
        return writableMap;
    }

    public static WritableArray services2Data(List<BluetoothGattService> services) {

        WritableArray writableArray = Arguments.createArray();
        for (BluetoothGattService service : services) {
            writableArray.pushMap(BluetoothUtils.service2Data(service));
        }

        return writableArray;
    }

    public static WritableMap peripheral2Data(BluetoothDevice device) {

        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("identifier", device.getAddress());
        writableMap.putString("name", device.getName());
        writableMap.putInt("type", device.getType());

        // TODO: merge data
        writableMap.putNull("state");
        writableMap.putNull("services");
        return writableMap;
    }

    public static WritableMap peripheral2Data(BluetoothGatt server) {

        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("identifier", server.getDevice().getAddress());
        writableMap.putString("name", server.getDevice().getName());
        writableMap.putArray("services", BluetoothUtils.services2Data(server.getServices()));
        writableMap.putInt("state", server.getConnectionState(server.getDevice()));
        return writableMap;
    }

    public static WritableMap descriptor2Data(BluetoothGattDescriptor descriptor) {

        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("uuid", descriptor.getUuid().toString());
        writableMap.putString("value", descriptor.getValue().toString());
        writableMap.putInt("permissions", descriptor.getPermissions());
        return writableMap;
    }

    public static WritableArray descriptors2Data(List<BluetoothGattDescriptor> descriptors) {

        WritableArray writableArray = Arguments.createArray();
        for (BluetoothGattDescriptor descriptor : descriptors) {
            writableArray.pushMap(BluetoothUtils.descriptor2Data(descriptor));
        }

        return writableArray;
    }
}
