package com.emanon.bluetooth.central;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import android.text.TextUtils;
import android.util.Log;

import com.emanon.bluetooth.common.Constants;
import com.emanon.bluetooth.listener.OnBluetoothStateChangedListener;
import com.emanon.bluetooth.receiver.BluetoohBroadcastReceiver;
import com.emanon.bluetooth.util.BluetoothUtils;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.ArrayList;
import java.util.List;

import static android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;

public class CertralManagerModule extends ReactContextBaseJavaModule implements OnBluetoothStateChangedListener {

    private static final String TAG = CertralManagerModule.class.getSimpleName();

    private final ReactApplicationContext mReactContext;

    private BluetoothManager mBluetoothManager;

    private BluetoothAdapter mBluetoothAdapter;

    private BluetoothLeScanner mBluetoothLeScanner;

    private List<BluetoothDevice> mDiscoveredDevices = new ArrayList<>();

    // 关联远程设备及相关服务等
    private List<BluetoothGatt> mConnectedGatt = new ArrayList<>();

    public CertralManagerModule(ReactApplicationContext mReactContext) {
        super(mReactContext);
        this.mReactContext = mReactContext;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

        this.mReactContext.registerReceiver(new BluetoohBroadcastReceiver(this),
                intentFilter);
    }

    @Override
    public void initialize() {
        super.initialize();

        if (mBluetoothAdapter != null) {
            onBluetoothStateChanged();
        }
    }

    @Override
    public String getName() {
        return "CentralManagerModule";
    }

    // Privae API

    public BluetoothDevice getDiscoveredDevice(ReadableMap deviceInfo) {
        for (BluetoothDevice device : mDiscoveredDevices) {
            if (device.getAddress().toString().equalsIgnoreCase(deviceInfo.getString("identifier"))) {
                return device;
            }
        }
        return null;
    }

    public BluetoothGatt getConnectedGatt(ReadableMap deviceInfo) {
        for (BluetoothGatt bluetoothGatt : mConnectedGatt) {
            if ((bluetoothGatt.getDevice().getAddress()).toString().equalsIgnoreCase(deviceInfo.getString("identifier"))) {
                return bluetoothGatt;
            }
        }
        return null;
    }

    public BluetoothGattService getService(BluetoothGatt gatt, ReadableMap serviceInfo) {
        for (BluetoothGattService service : gatt.getServices()) {
            if (service.getUuid().toString().equalsIgnoreCase(serviceInfo.getString("uuid"))) {
                return service;
            }
        }
        return null;
    }

    public BluetoothGattCharacteristic getCharacteristic(BluetoothGattService service, ReadableMap characteristicInfo) {
        for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
            if (characteristic.getUuid().toString().equalsIgnoreCase(characteristicInfo.getString("uuid"))) {
                return characteristic;
            }
        }
        return null;
    }

    public BluetoothGattDescriptor getDescriptor(BluetoothGattCharacteristic characteristic, ReadableMap descriptorInfo) {

        for (BluetoothGattDescriptor descriptor : characteristic.getDescriptors()) {
            if (descriptor.getUuid().toString().equalsIgnoreCase(descriptorInfo.getString("uuid"))) {
                return descriptor;
            }
        }
        return null;
    }

    public void sendEvent(String eventName,
                          Object params) {
        mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    // CentralManager API

    @ReactMethod
    public void createCentralManager(ReadableMap options) {
        mBluetoothManager =
                (BluetoothManager) this.mReactContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
    }

    @ReactMethod
    public void startScan(ReadableMap options) {
        ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build();

        ArrayList<ScanFilter> filters = new ArrayList<>();

        ReadableArray serviceUuids = options.getArray("serviceUUIDs");
        for (int i = 0; i < serviceUuids.size(); i++) {
            ScanFilter.Builder builder = new ScanFilter.Builder();
            builder.setServiceUuid(ParcelUuid.fromString(serviceUuids.getString(i)));
            filters.add(builder.build());
        }

        mBluetoothLeScanner.startScan(filters, settings, mScanCallback);
    }

    @ReactMethod
    public void stopScan() {
        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    @ReactMethod
    public void connectPeripheral(ReadableMap deviceInfo) {
        BluetoothDevice device = getDiscoveredDevice(deviceInfo);
        device.connectGatt(mReactContext, false, mBluetoothGattCallback);
    }

    @ReactMethod
    public void disconnectPeripheral(ReadableMap deviceInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        bluetoothGatt.disconnect();
    }

    // Peripheral API
    @ReactMethod
    public void discoverServices(ReadableMap deviceInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        bluetoothGatt.discoverServices();
    }

    @ReactMethod
    public void discoverCharacteristics() {
        // Android No Need Call
    }

    @ReactMethod
    public void discoverDescriptorsForCharacteristic() {
        // Android No Need Call
    }

    @ReactMethod
    public void readCharacteristic(ReadableMap deviceInfo, ReadableMap serviceInfo, ReadableMap characteristicInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        BluetoothGattService service = getService(bluetoothGatt, serviceInfo);
        BluetoothGattCharacteristic characteristic = getCharacteristic(service, characteristicInfo);
        bluetoothGatt.readCharacteristic(characteristic);
    }

    @ReactMethod
    public void writeCharacteristic(ReadableMap deviceInfo, ReadableMap serviceInfo, ReadableMap characteristicInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        BluetoothGattService service = getService(bluetoothGatt, serviceInfo);
        BluetoothGattCharacteristic characteristic = getCharacteristic(service, characteristicInfo);

        characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
        characteristic.setValue(characteristicInfo.getString("value"));
        bluetoothGatt.writeCharacteristic(characteristic);
    }

    @ReactMethod
    public void readDescriptor(ReadableMap deviceInfo, ReadableMap serviceInfo, ReadableMap characteristicInfo, ReadableMap descriptorInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        BluetoothGattService service = getService(bluetoothGatt, serviceInfo);
        BluetoothGattCharacteristic characteristic = getCharacteristic(service, characteristicInfo);
        BluetoothGattDescriptor descriptor = getDescriptor(characteristic, descriptorInfo);
        bluetoothGatt.readDescriptor(descriptor);
    }

    @ReactMethod
    public void writeDescriptor(ReadableMap deviceInfo, ReadableMap serviceInfo, ReadableMap characteristicInfo, ReadableMap descriptorInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        BluetoothGattService service = getService(bluetoothGatt, serviceInfo);
        BluetoothGattCharacteristic characteristic = getCharacteristic(service, characteristicInfo);
        BluetoothGattDescriptor descriptor = getDescriptor(characteristic, descriptorInfo);
        descriptor.setValue(descriptorInfo.getString("value").getBytes());
        bluetoothGatt.writeDescriptor(descriptor);
    }

    @ReactMethod
    public void subscribe(ReadableMap deviceInfo, ReadableMap serviceInfo, ReadableMap characteristicInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        BluetoothGattService service = getService(bluetoothGatt, serviceInfo);
        BluetoothGattCharacteristic characteristic = getCharacteristic(service, characteristicInfo);
        boolean result = bluetoothGatt.setCharacteristicNotification(characteristic, true);

        for (BluetoothGattDescriptor descriptor : characteristic.getDescriptors()) {
            if (descriptor.getUuid().toString().equalsIgnoreCase(Constants.CCCD)) {
                // 需要写入属性以允许通知
                descriptor.setValue(ENABLE_NOTIFICATION_VALUE);
                bluetoothGatt.writeDescriptor(descriptor);
            }
        }

        sendEvent(Constants.CentralEvent.didUpdateNotificationStateForCharacteristic, BluetoothUtils.characteristic2Data(characteristic));
    }

    @ReactMethod
    public void unsubscribe(ReadableMap deviceInfo, ReadableMap serviceInfo, ReadableMap characteristicInfo) {
        BluetoothGatt bluetoothGatt = getConnectedGatt(deviceInfo);
        BluetoothGattService service = getService(bluetoothGatt, serviceInfo);
        BluetoothGattCharacteristic characteristic = getCharacteristic(service, characteristicInfo);
        bluetoothGatt.setCharacteristicNotification(characteristic, false);
        sendEvent(Constants.CentralEvent.didUpdateNotificationStateForCharacteristic, BluetoothUtils.characteristic2Data(characteristic));
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            Log.d(TAG, "onScanResult ");
            onDeviceScaned(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            Log.d(TAG, "onBatchScanResults " + results.size());

            for (ScanResult result : results) {
                Log.d(TAG, "onScanResult " + result.getDevice().getName());
                onDeviceScaned(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d(TAG, "onScanFailed " + errorCode);
        }
    };

    private void onDeviceScaned(ScanResult result) {
        Log.d(TAG, "onDeviceScaned " + result.getDevice().getName() + " : " + result.getDevice().getAddress() + " : " + result.getDevice().getType());

        if (!TextUtils.isEmpty(result.getDevice().getAddress())) {

            boolean duplicate = false;
            for (BluetoothDevice device : this.mDiscoveredDevices) {
                if (device.getAddress().equals(result.getDevice().getAddress())) {
                    duplicate = true;
                    break;
                }
            }

            if (!duplicate) {
                Log.d(TAG, ">>>>Found a new Device");
                this.mDiscoveredDevices.add(result.getDevice());
                sendEvent(Constants.CentralEvent.didDiscoverPeripheral, BluetoothUtils.peripheral2Data(result.getDevice()));
            }
        }
    }

    private BluetoothGattCallback mBluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(gatt, txPhy, rxPhy, status);
            Log.d(TAG, "onPhyUpdate");
        }

        @Override
        public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyRead(gatt, txPhy, rxPhy, status);
            Log.d(TAG, "onPhyRead");
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d(TAG, "onConnectionStateChange address:" + gatt.getDevice().getAddress() + ", status : " + status + ", newState : " + newState);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {

                    mConnectedGatt.add(gatt);
                    sendEvent(Constants.CentralEvent.didConnectPeripheral, BluetoothUtils.peripheral2Data(gatt.getDevice()));

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

                    if (mConnectedGatt.contains(gatt)) {
                        mConnectedGatt.remove(gatt);
                        sendEvent(Constants.CentralEvent.didDisconnectPeripheral, BluetoothUtils.peripheral2Data(gatt.getDevice()));
                    }
                } else {
                    sendEvent(Constants.CentralEvent.didFailToConnectPeripheral, BluetoothUtils.peripheral2Data(gatt.getDevice()));
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(TAG, "onServicesDiscovered");
            super.onServicesDiscovered(gatt, status);

            if (status == BluetoothGatt.GATT_SUCCESS) {

                List<BluetoothGattService> services = gatt.getServices();
                sendEvent(Constants.CentralEvent.didDiscoverServices, BluetoothUtils.services2Data(services));

                for (BluetoothGattService service : services) {

                    List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                    sendEvent(Constants.CentralEvent.didDiscoverCharacteristicsForService, BluetoothUtils.characteristics2Data(characteristics));

                    for (BluetoothGattCharacteristic characteristic : characteristics) {

                        List<BluetoothGattDescriptor> descriptors = characteristic.getDescriptors();
                        sendEvent(Constants.CentralEvent.didDiscoverDescriptorsForCharacteristic, BluetoothUtils.descriptors2Data(descriptors));

                        for (BluetoothGattDescriptor descriptor : descriptors) {
                            Log.d(TAG, "service ==> " + service.getUuid().toString() + "characteristic ==> " + characteristic.getUuid().toString() + " descriptor ==> " + descriptor.getUuid().toString() + " : " + (descriptor.getValue() != null ? descriptor.getValue().toString() : null));
                        }
                    }
                }
            } else {

            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicRead :" + characteristic.getUuid().toString() + ", status : " + status + ", value : " + characteristic.getValue());
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicWrite :" + characteristic.getUuid().toString() + ", status : " + status + ", value : " + characteristic.getValue());
            sendEvent(Constants.CentralEvent.didWriteValueForCharacteristic, BluetoothUtils.characteristic2Data(characteristic));
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d(TAG, "onCharacteristicChanged :" + characteristic.getUuid().toString() + ", value : " + characteristic.getStringValue(0));
            sendEvent(Constants.CentralEvent.didUpdateValueForCharacteristic, BluetoothUtils.characteristic2Data(characteristic));
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            Log.w(TAG, "onDescriptorRead: " + descriptor.getUuid() + " : " + descriptor.getValue().toString());
            sendEvent(Constants.CentralEvent.didUpdateValueForDescriptor, BluetoothUtils.descriptor2Data(descriptor));
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            Log.w(TAG, "onDescriptorWrite: " + descriptor.getCharacteristic().getUuid() + " / " + descriptor.getUuid() + " : " + descriptor.getValue().toString());
            sendEvent(Constants.CentralEvent.didWriteValueForDescriptor, BluetoothUtils.descriptor2Data(descriptor));
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            Log.d(TAG, "onReliableWriteCompleted");
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Log.d(TAG, "onReadRemoteRssi");
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            Log.d(TAG, "onMtuChanged : " + mtu + ", status : " + status);
        }
    };

    @Override
    public void onBluetoothStateChanged() {

        int bluetoothState = mBluetoothAdapter.getState();

        Log.d(TAG, "onBluetoothStateChanged : " + bluetoothState);

        switch (bluetoothState) {
            case BluetoothAdapter.STATE_TURNING_OFF:
                bluetoothState = Constants.MANAGER_STATE_POWERED_OFF;
                break;
            case BluetoothAdapter.STATE_OFF:
                bluetoothState = Constants.MANAGER_STATE_POWERED_OFF;
                break;
            case BluetoothAdapter.STATE_ON:
                bluetoothState = Constants.MANAGER_STATE_POWERED_ON;
                break;
            default:
                return;
        }

        WritableMap body = Arguments.createMap();
        body.putInt("state", bluetoothState);

        sendEvent(Constants.CentralEvent.didUpdateState, body);
    }
}
