package com.emanon.bluetooth.peripheral;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import android.util.Log;

import com.emanon.bluetooth.common.Constants;
import com.emanon.bluetooth.listener.OnBluetoothStateChangedListener;
import com.emanon.bluetooth.receiver.BluetoohBroadcastReceiver;
import com.emanon.bluetooth.util.BluetoothUtils;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PeripheralManagerModule extends ReactContextBaseJavaModule implements OnBluetoothStateChangedListener {

    private final static String TAG = PeripheralManagerModule.class.getSimpleName();

    private final ReactApplicationContext mReactContext;

    private BluetoothManager mBluetoothManager;

    private BluetoothAdapter mBluetoothAdapter;

    private BluetoothGattServer mBluetoothGattServer;

    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;

    private final List<BluetoothGattDescriptor> mDescriptors = new ArrayList<>();

    private final List<BluetoothGattCharacteristic> mCharacteristics = new ArrayList<>();

    private final List<BluetoothGattService> mServices = new ArrayList<>();

    private final List<BluetoothDevice> mCentrals = new ArrayList<>();

    public PeripheralManagerModule(ReactApplicationContext mReactContext) {
        super(mReactContext);
        this.mReactContext = mReactContext;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

        this.mReactContext.registerReceiver(new BluetoohBroadcastReceiver(this),
                intentFilter);
    }

    @Override
    public void initialize() {
        super.initialize();

        if (mBluetoothAdapter != null) {
            onBluetoothStateChanged();
        }
    }

    @Override
    public String getName() {
        return "PeripheralManagerModule";
    }

    // Private API

    public BluetoothGattService getService(ReadableMap serviceInfo) {
        for (BluetoothGattService service : mServices) {
            if (service.getUuid().toString().equalsIgnoreCase(serviceInfo.getString("uuid"))) {
                return service;
            }
        }
        return null;
    }

    public BluetoothGattCharacteristic getCharacteristic(ReadableMap characteristicInfo) {
        for (BluetoothGattCharacteristic characteristic : mCharacteristics) {
            if (characteristic.getUuid().toString().equalsIgnoreCase(characteristicInfo.getString("uuid"))) {
                return characteristic;
            }
        }
        return null;
    }

    public BluetoothDevice getCentral(ReadableMap deviceInfo) {
        for (BluetoothDevice device : mCentrals) {
            if (device.getAddress().equalsIgnoreCase(deviceInfo.getString("identifier"))) {
                return device;
            }
        }
        return null;
    }

    private void sendEvent(String eventName,
                           Object params) {
        mReactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    public static AdvertiseSettings createAdvSettings(boolean connectable, int timeoutMillis) {
        AdvertiseSettings.Builder builder = new AdvertiseSettings.Builder();
        //设置广播的模式,应该是跟功耗相关
        builder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED);
        builder.setConnectable(connectable);
        builder.setTimeout(timeoutMillis);
        builder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);
        return builder.build();
    }

    //设置一下FMP广播数据
    public static AdvertiseData createFMPAdvertiseData(ReadableMap advertiseData) {
        AdvertiseData.Builder builder = new AdvertiseData.Builder();
        builder.setIncludeDeviceName(true);

        ReadableArray uuids = advertiseData.getArray("serviceUUIDs");
        if (uuids != null) {
            for (int i = 0; i < uuids.size(); i++) {
                builder.addServiceUuid(ParcelUuid.fromString(uuids.getString(i)));
            }
        }
        return builder.build();
    }


    // Public API

    @ReactMethod
    public void createPeripheralManager(ReadableMap options) {
        mBluetoothManager =
                (BluetoothManager) this.mReactContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        mBluetoothGattServer = mBluetoothManager.openGattServer(mReactContext, mBluetoothGattServerCallback);

        mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
    }

    @ReactMethod
    public void createDescriptor(ReadableMap descriptorInfo, Promise promise) {
        BluetoothGattDescriptor descriptor = new BluetoothGattDescriptor(UUID.fromString(descriptorInfo.getString("uuid")),
                descriptorInfo.getInt("permissions"));
        mDescriptors.add(descriptor);
        promise.resolve(BluetoothUtils.descriptor2Data(descriptor));
    }

    @ReactMethod
    public void createCharacteristic(ReadableMap characteristicInfo, Promise promise) {

        BluetoothGattCharacteristic characteristic = new BluetoothGattCharacteristic(
                UUID.fromString(characteristicInfo.getString("uuid")),
                characteristicInfo.getInt("properties"),
                characteristicInfo.getInt("permissions"));
        mCharacteristics.add(characteristic);

        promise.resolve(BluetoothUtils.characteristic2Data(characteristic));
    }

    @ReactMethod
    public void createService(ReadableMap serviceInfo, Promise promise) {

        try {
            BluetoothGattService service = new BluetoothGattService(
                    UUID.fromString(serviceInfo.getString("uuid")),
                    serviceInfo.getBoolean("primary") ? BluetoothGattService.SERVICE_TYPE_PRIMARY : BluetoothGattService.SERVICE_TYPE_SECONDARY);

            ReadableArray characteristicsInfo = serviceInfo.getArray("characteristics");

            if (characteristicsInfo != null) {
                for (int i = 0; i < characteristicsInfo.size(); i++) {
                    BluetoothGattCharacteristic characteristic = this.getCharacteristic(characteristicsInfo.getMap(i));
                    service.addCharacteristic(characteristic);
                }
            }

            mServices.add(service);
            promise.resolve(BluetoothUtils.service2Data(service));

        } catch (Exception e) {
            promise.reject(e);
            Log.e(TAG, "error", e);
        }
    }

    @ReactMethod
    public void addService(ReadableMap serviceInfo) {
        BluetoothGattService service = this.getService(serviceInfo);
        mBluetoothGattServer.addService(service);
    }

    @ReactMethod
    public void startAdvertise(ReadableMap advertiseData) {
        mBluetoothLeAdvertiser.startAdvertising(createAdvSettings(true, 0), createFMPAdvertiseData(advertiseData), mAdvertiseCallback);
    }

    @ReactMethod
    public void stopAdvertise() {
        mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
    }

    @ReactMethod
    public void updateValueForCharacteristicOnSubscribedCentrals(ReadableMap characteristicInfo, ReadableArray centralsInfo) {

        BluetoothGattCharacteristic characteristic = this.getCharacteristic(characteristicInfo);
        characteristic.setValue(characteristicInfo.getString("value"));

        for (int i = 0; i < centralsInfo.size(); i++) {
            ReadableMap centralInfo = centralsInfo.getMap(i);
            BluetoothDevice device = getCentral(centralInfo);
            mBluetoothGattServer.notifyCharacteristicChanged(device, characteristic, false);
        }
    }

    @ReactMethod
    public void sendResponse(ReadableMap responseInfo) {

        BluetoothDevice device = getCentral(responseInfo.getMap("peripheral"));
        int requestId = responseInfo.getInt("requestId");
        int status = responseInfo.getInt("status");
        int offset = responseInfo.getInt("offset");
        String value = responseInfo.getString("value");

        mBluetoothGattServer.sendResponse(device, requestId, status, offset, value == null ? null : value.getBytes());
    }

    // AdvertiseCallback

    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            if (settingsInEffect != null) {
                Log.d(TAG, "onStartSuccess TxPowerLv=" + settingsInEffect.getTxPowerLevel() + " mode=" + settingsInEffect.getMode() + " timeout=" + settingsInEffect.getTimeout());
            } else {
                Log.d(TAG, "onStartSuccess, settingInEffect is null");
            }

            WritableMap result = Arguments.createMap();
            result.putBoolean("success", true);
            result.putNull("error");
            sendEvent(Constants.PeripheralEvent.didStartAdvertising, result);
        }

        public void onStartFailure(int errorCode) {
            Log.d(TAG, "onStartFailure errorCode=" + errorCode);
            WritableMap result = Arguments.createMap();
            result.putBoolean("success", false);

            switch (errorCode) {
                case ADVERTISE_FAILED_DATA_TOO_LARGE:
                    result.putString("error", "ADVERTISE_FAILED_DATA_TOO_LARGE");
                    break;
                case ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                    result.putString("error", "ADVERTISE_FAILED_TOO_MANY_ADVERTISERS");
                    break;
                case ADVERTISE_FAILED_ALREADY_STARTED:
                    result.putString("error", "ADVERTISE_FAILED_ALREADY_STARTED");
                    break;
                case ADVERTISE_FAILED_INTERNAL_ERROR:
                    result.putString("error", "ADVERTISE_FAILED_INTERNAL_ERROR");
                    break;
                case ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                    result.putString("error", "ADVERTISE_FAILED_FEATURE_UNSUPPORTED");
                    break;
                default:
                    break;
            }
            result.putInt("errorCode", errorCode);
            sendEvent(Constants.PeripheralEvent.didStartAdvertising, result);
        }
    };

    // BluetoothGattServerCallback

    private BluetoothGattServerCallback mBluetoothGattServerCallback = new BluetoothGattServerCallback() {
        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            super.onConnectionStateChange(device, status, newState);
            Log.d(TAG, String.format("onConnectionStateChange device: %s, status : %d, newState : %d", device.getAddress(), status, newState));
            if (status == BluetoothGatt.GATT_SUCCESS) {

                switch (newState) {
                    case BluetoothProfile.STATE_DISCONNECTED:
                        mCentrals.remove(device);
                        sendEvent(Constants.PeripheralEvent.didCentralDisonnected, BluetoothUtils.peripheral2Data(device));
                        break;
                    case BluetoothProfile.STATE_CONNECTING:
                        break;
                    case BluetoothProfile.STATE_CONNECTED:
                        if (!mCentrals.contains(device)) {
                            mCentrals.add(device);
                            sendEvent(Constants.PeripheralEvent.didCentralConnected, BluetoothUtils.peripheral2Data(device));
                        }
                        break;
                    case BluetoothProfile.STATE_DISCONNECTING:
                        break;
                    default:
                        break;
                }
            } else {

            }
        }

        @Override
        public void onServiceAdded(int status, BluetoothGattService service) {
            super.onServiceAdded(status, service);
            Log.d(TAG, "onServiceAdded");
            sendEvent(Constants.PeripheralEvent.didAddService, BluetoothUtils.service2Data(service));
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
            Log.d(TAG, "onCharacteristicReadRequest");
            mBluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, null);
            sendEvent(Constants.PeripheralEvent.didReceiveReadRequest, null);
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId, BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value);
            Log.d(TAG, "onCharacteristicWriteRequest : " + new String(value));
            mBluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value);
            sendEvent(Constants.PeripheralEvent.didReceiveWriteRequests, BluetoothUtils.characteristic2Data(characteristic));
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattDescriptor descriptor) {
            super.onDescriptorReadRequest(device, requestId, offset, descriptor);
            Log.d(TAG, "onDescriptorReadRequest");
            mBluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, null);
            sendEvent(Constants.PeripheralEvent.didReceiveReadRequest, BluetoothUtils.descriptor2Data(descriptor));
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId, BluetoothGattDescriptor descriptor, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
            Log.d(TAG, "onDescriptorWriteRequest");
            mBluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value);
            sendEvent(Constants.PeripheralEvent.didReceiveWriteRequests, BluetoothUtils.descriptor2Data(descriptor));
        }

        @Override
        public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
            super.onExecuteWrite(device, requestId, execute);
            Log.d(TAG, "onExecuteWrite");
        }

        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            super.onNotificationSent(device, status);
            Log.d(TAG, "onNotificationSent");
        }

        @Override
        public void onMtuChanged(BluetoothDevice device, int mtu) {
            super.onMtuChanged(device, mtu);
            Log.d(TAG, "onMtuChanged");
        }

        @Override
        public void onPhyUpdate(BluetoothDevice device, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(device, txPhy, rxPhy, status);
            Log.d(TAG, "onPhyUpdate");
        }

        @Override
        public void onPhyRead(BluetoothDevice device, int txPhy, int rxPhy, int status) {
            super.onPhyRead(device, txPhy, rxPhy, status);
            Log.d(TAG, "onPhyRead");
        }
    };

    @Override
    public void onBluetoothStateChanged() {

        int bluetoothState = mBluetoothAdapter.getState();

        Log.d(TAG, "onBluetoothStateChanged : " + bluetoothState);

        switch (bluetoothState) {
            case BluetoothAdapter.STATE_TURNING_OFF:
                bluetoothState = Constants.MANAGER_STATE_POWERED_OFF;
                break;
            case BluetoothAdapter.STATE_OFF:
                bluetoothState = Constants.MANAGER_STATE_POWERED_OFF;
                break;
            case BluetoothAdapter.STATE_ON:
                bluetoothState = Constants.MANAGER_STATE_POWERED_ON;
                break;
            default:
                return;
        }

        WritableMap body = Arguments.createMap();
        body.putInt("state", bluetoothState);

        sendEvent(Constants.PeripheralEvent.didUpdateState, body);
    }
}
