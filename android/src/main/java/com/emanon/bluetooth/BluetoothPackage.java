package com.emanon.bluetooth;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.emanon.bluetooth.central.CertralManagerModule;
import com.emanon.bluetooth.peripheral.PeripheralManagerModule;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

public class BluetoothPackage implements ReactPackage {
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {

        return Arrays.<NativeModule>asList(new BluetoothModule(reactContext),
                new CertralManagerModule(reactContext),
                new PeripheralManagerModule(reactContext));
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}
