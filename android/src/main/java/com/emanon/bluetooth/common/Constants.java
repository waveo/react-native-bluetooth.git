package com.emanon.bluetooth.common;

public class Constants {

    public static class CentralEvent {
        public static final String didUpdateState = "centralManagerDidUpdateState";
        public static final String willRestoreState = "centralManagerWillRestoreState";
        public static final String didDiscoverPeripheral = "didDiscoverPeripheral";
        public static final String didConnectPeripheral = "didConnectPeripheral";
        public static final String didDisconnectPeripheral = "didDisconnectPeripheral";
        public static final String didFailToConnectPeripheral = "didFailToConnectPeripheral";
        public static final String didUpdateName = "didUpdateName";
        public static final String didModifyServices = "didModifyServices";
        public static final String didUpdateRSSI = "didUpdateRSSI";
        public static final String didReadRSSI = "didReadRSSI";
        public static final String didDiscoverServices = "didDiscoverServices";
        public static final String didDiscoverIncludedServicesForService = "didDiscoverIncludedServicesForService";
        public static final String didDiscoverCharacteristicsForService = "didDiscoverCharacteristicsForService";
        public static final String didUpdateValueForCharacteristic = "didUpdateValueForCharacteristic";
        public static final String didWriteValueForCharacteristic = "didWriteValueForCharacteristic";
        public static final String didUpdateNotificationStateForCharacteristic = "didUpdateNotificationStateForCharacteristic";
        public static final String didDiscoverDescriptorsForCharacteristic = "didDiscoverDescriptorsForCharacteristic";
        public static final String didUpdateValueForDescriptor = "didUpdateValueForDescriptor";
        public static final String didWriteValueForDescriptor = "didWriteValueForDescriptor";

        // L2CAP Event
        public static final String isReadyToSendWriteWithoutResponse = "isReadyToSendWriteWithoutResponse";
        public static final String didOpenL2CAPChannel = "peripheralDidOpenL2CAPChannel";
    }

    public static class PeripheralEvent {
        public static final String didUpdateState = "peripheralManagerDidUpdateState";
        public static final String willRestoreState = "peripheralManagerWillRestoreState";  // iOS Only
        public static final String didAddService = "didAddService";
        public static final String didStartAdvertising = "didStartAdvertising";
        public static final String didCentralConnected = "didCentralConnected";  // Android Only
        public static final String didCentralDisonnected = "didCentralDisonnected"; // Android Only
        public static final String didSubscribeToCharacteristic = "didSubscribeToCharacteristic";   // iOS Only
        public static final String didUnsubscribeFromCharacteristic = "didUnsubscribeFromCharacteristic";   // iOS Only
        public static final String didReceiveReadRequest = "didReceiveReadRequest";
        public static final String didReceiveWriteRequests = "didReceiveWriteRequests";
        public static final String isReadyToUpdateSubscribers = "isReadyToUpdateSubscribers";   // iOS Only

        // L2CAP Event
        public static final String didPublishL2CAPChannel = "didPublishL2CAPChannel";   // iOS Only
        public static final String didUnpublishL2CAPChannel = "didUnpublishL2CAPChannel";   // iOS Only
        public static final String didOpenL2CAPChannel = "peripheralManagerDidOpenL2CAPChannel";    // iOS Only
    }

    // Client Characteristic Configuration Descriptor UUID
    public static final String CCCD = "00002902-0000-1000-8000-00805f9b34fb";

    public static final int MANAGER_STATE_POWERED_OFF = 10;

    public static final int MANAGER_STATE_POWERED_ON = 12;
}
