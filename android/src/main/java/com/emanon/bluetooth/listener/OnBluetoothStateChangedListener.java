package com.emanon.bluetooth.listener;

public interface OnBluetoothStateChangedListener {
  void onBluetoothStateChanged();
}
