import { NativeModules, DeviceEventEmitter } from 'react-native';

export default class PeripheralManager {

  constructor() {

    this.NativeModule = NativeModules.PeripheralManagerModule;

    DeviceEventEmitter.addListener("peripheralManagerDidUpdateState", this.onPeripheralManagerDidUpdateState);
    DeviceEventEmitter.addListener("peripheralManagerWillRestoreState", this.onPeripheralManagerWillRestoreState);
    DeviceEventEmitter.addListener("didAddService", this.onDidAddService);
    DeviceEventEmitter.addListener("didStartAdvertising", this.onDidStartAdvertising);
    DeviceEventEmitter.addListener("didSubscribeToCharacteristic", this.onDidSubscribeToCharacteristic);
    DeviceEventEmitter.addListener("didUnsubscribeFromCharacteristic", this.onDidUnsubscribeFromCharacteristic);
    DeviceEventEmitter.addListener("didReceiveReadRequest", this.onDidReceiveReadRequest);
    DeviceEventEmitter.addListener("didReceiveWriteRequests", this.onDidReceiveWriteRequests);
    DeviceEventEmitter.addListener("isReadyToUpdateSubscribers", this.onIsReadyToUpdateSubscribers);
    DeviceEventEmitter.addListener("didPublishL2CAPChannel", this.onDidPublishL2CAPChannel);
    DeviceEventEmitter.addListener("didUnpublishL2CAPChannel", this.onDidUnpublishL2CAPChannel);
    DeviceEventEmitter.addListener("peripheralManagerDidOpenL2CAPChannel", this.onPeripheralManagerDidOpenL2CAPChannel);
  }

  onPeripheralManagerDidUpdateState = (event) => {} 
  onPeripheralManagerWillRestoreState = (event) => {} 
  onDidAddService = (event) => {} 

  onDidStartAdvertising = (event) => {
    if (event.success == true) {
      this._advertising = true;
    } else {
      this._advertising = false;
    }
  } 

  onDidSubscribeToCharacteristic = (event) => {} 
  onDidUnsubscribeFromCharacteristic = (event) => {} 
  onDidReceiveReadRequest = (event) => {} 
  onDidReceiveWriteRequests = (event) => {} 
  onIsReadyToUpdateSubscribers = (event) => {} 
  onDidPublishL2CAPChannel = (event) => {} 
  onDidUnpublishL2CAPChannel = (event) => {} 
  onPeripheralManagerDidOpenL2CAPChannel = (event) => {} 

  // Getter & Setter

  get advertising () {
    return this._advertising;
  }

  // Public API

  addService() {

  }

  startAdvertising () {

  }

  stopAdvertising() {

  }

  respondToRequestWithResult() {

  }

  updateValueForCharacteristicOnSubscribedCentrals () {
    
  }
}