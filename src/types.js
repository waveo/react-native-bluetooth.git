
// iOS
export const CBCharacteristicWriteType  = {
  CharacteristicWriteWithResponse = 0,
  CharacteristicWriteWithoutResponse = 1,
}

export const CBCharacteristicProperties = {
  CBCharacteristicPropertyBroadcast												= 0x01,
	CBCharacteristicPropertyRead													= 0x02,
	CBCharacteristicPropertyWriteWithoutResponse									= 0x04,
	CBCharacteristicPropertyWrite													= 0x08,
	CBCharacteristicPropertyNotify													= 0x10,
	CBCharacteristicPropertyIndicate												= 0x20,
	CBCharacteristicPropertyAuthenticatedSignedWrites								= 0x40,
	CBCharacteristicPropertyExtendedProperties										= 0x80,
	CBCharacteristicPropertyNotifyEncryptionRequired = 0x100,
	CBCharacteristicPropertyIndicateEncryptionRequired 	= 0x200
}

export const CBAttributePermissions = {
	CBAttributePermissionsReadable					= 0x01,
	CBAttributePermissionsWriteable					= 0x02,
	CBAttributePermissionsReadEncryptionRequired	= 0x04,
  CBAttributePermissionsWriteEncryptionRequired	= 0x08
}

export const CBManagerState = {
	CBManagerStateUnknown = 0,
	CBManagerStateResetting = 1,
	CBManagerStateUnsupported = 2,
	CBManagerStateUnauthorized = 3,
	CBManagerStatePoweredOff = 4,
  CBManagerStatePoweredOn = 5,
}

// Android

export const BluetootAdapterState = {
	STATE_OFF = 10,
	STATE_ON = 12,
	STATE_TURNING_OFF = 13,
	STATE_TURNING_ON = 11,
}

export const BluetoothGattCharacteristicProperty = {
	PROPERTY_BROADCAST = 0x01,
	PROPERTY_READ = 0x02,
	PROPERTY_WRITE_NO_RESPONSE = 0x04,
	PROPERTY_WRITE = 0x08,
	PROPERTY_NOTIFY = 0x10,
	PROPERTY_INDICATE = 0x20,
	PROPERTY_SIGNED_WRITE = 0x40,
	PROPERTY_EXTENDED_PROPS = 0x80,
}

export const BluetoothGattCharacteristicPermission = {
	PERMISSION_READ = 0x01,
	PERMISSION_READ_ENCRYPTED = 0x02,
	PERMISSION_READ_ENCRYPTED_MITM = 0x04,
	PERMISSION_WRITE = 0x10,
	PERMISSION_WRITE_ENCRYPTED = 0x20,
	PERMISSION_WRITE_ENCRYPTED_MITM = 0x40,
	PERMISSION_WRITE_SIGNED = 0x80,
	PERMISSION_WRITE_SIGNED_MITM = 0x100,
}

export const BluetoothGattCharacteristicWriteType = {
	WRITE_TYPE_DEFAULT = 0x02,
	WRITE_TYPE_NO_RESPONSE = 0x01,
	WRITE_TYPE_SIGNED = 0x04,
}

export const BluetoothGattCharacteristicFormat = {
	FORMAT_UINT8 = 0x11,
	FORMAT_UINT16 = 0x12,
	FORMAT_UINT32 = 0x14,
	FORMAT_SINT8 = 0x21,
	FORMAT_SINT16 = 0x22,
	FORMAT_SINT32 = 0x24,
	FORMAT_SFLOAT = 0x32,
	FORMAT_FLOAT = 0x34,
}

