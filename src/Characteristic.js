import { NativeModules } from 'react-native';

export default class Characteristic {

  constructor(props) {
    
    this.NativeModule = NativeModules.PeripheralManagerModule;

    this._uuid = props.uuid;
    this._properties = props.properties;
    this._permissions = props.permissions;
  }

}