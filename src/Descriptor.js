import { NativeModules } from 'react-native';

export default class Descriptor {
  constructor(props) {

    this.NativeModule = NativeModules.PeripheralManagerModule;
    
    this._uuid = props.uuid;
  }

  get uuid() {
    return this._uuid;
  } 
}
