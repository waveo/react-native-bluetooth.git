import { NativeModules } from 'react-native';

export default class Central {
  constructor(props) {
    this._uuid = props.uuid;
  }

  get uuid() {
    return this._uuid;
  } 

  set uuid(uuid) {
    this._uuid = uuid;
  }
}
