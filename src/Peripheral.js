import { NativeModules, DeviceEventEmitter } from 'react-native';

export default class Peripheral {

  constructor(props) {
    this.NativeModule = NativeModules.CentralManagerModule;
    this._name = props.name;
    this._identifier = props.identifier;
    this._rssi = props.rssi;
    this._state = props.state;
    this._service = new Array();

    DeviceEventEmitter.addListener("didUpdateName", this.onDidUpdateName);
    DeviceEventEmitter.addListener("didModifyServices", this.onDidModifyServices);
    DeviceEventEmitter.addListener("didUpdateRSSI", this.onDidUpdateRSSI);
    DeviceEventEmitter.addListener("didReadRSSI", this.onDidReadRSSI);
    DeviceEventEmitter.addListener("didDiscoverServices", this.onDidDiscoverServices);
    DeviceEventEmitter.addListener("didDiscoverIncludedServicesForService", this.onDidDiscoverIncludedServicesForService);
    DeviceEventEmitter.addListener("didDiscoverCharacteristicsForService", this.onDidDiscoverCharacteristicsForService);
    DeviceEventEmitter.addListener("didUpdateValueForCharacteristic", this.onDidUpdateValueForCharacteristic);
    DeviceEventEmitter.addListener("didWriteValueForCharacteristic", this.onDidWriteValueForCharacteristic);
    DeviceEventEmitter.addListener("didUpdateNotificationStateForCharacteristic", this.onDidUpdateNotificationStateForCharacteristic);
    DeviceEventEmitter.addListener("didDiscoverDescriptorsForCharacteristic", this.onDidDiscoverDescriptorsForCharacteristic);
    DeviceEventEmitter.addListener("didUpdateValueForDescriptor", this.onDidUpdateValueForDescriptor);
    DeviceEventEmitter.addListener("didWriteValueForDescriptor", this.onDidWriteValueForDescriptor);
    DeviceEventEmitter.addListener("isReadyToSendWriteWithoutResponse", this.onIsReadyToSendWriteWithoutResponse);
    DeviceEventEmitter.addListener("peripheralDidOpenL2CAPChannel", this.onPeripheralDidOpenL2CAPChannel);
  }

  onDidUpdateName = (event) => {}
  onDidModifyServices = (event) => {}
  onDidUpdateRSSI = (event) => {}
  onDidReadRSSI = (event) => {}
  onDidDiscoverServices = (event) => {}
  onDidDiscoverIncludedServicesForService = (event) => {}
  onDidDiscoverCharacteristicsForService = (event) => {}
  onDidUpdateValueForCharacteristic = (event) => {}
  onDidWriteValueForCharacteristic = (event) => {}
  onDidUpdateNotificationStateForCharacteristic = (event) => {}
  onDidDiscoverDescriptorsForCharacteristic = (event) => {}
  onDidUpdateValueForDescriptor = (event) => {}
  onDidWriteValueForDescriptor = (event) => {}
  onIsReadyToSendWriteWithoutResponse = (event) => {}
  onPeripheralDidOpenL2CAPChannel = (event) => {}

  addCharacteristic() {
    
  }

  get uuid () {
    return this._uuid;
  }

  get identifier () {
    return this._identifier;
  }

  get rssi() {
    return this._rssi;
  }

  get state() {
    return this._state;
  }

  set uuid () {
    return this._uuid;
  }

  set identifier () {
    return this._identifier;
  }

  set rssi() {
    return this._rssi;
  }

  set state() {
    return this._state;
  }

  discoverServices = () => {

  }

  discoverCharacteristicForService = (service) => {

  }

  discoverDescriptorForCharacteristic = (characteristic) => {

  }

}