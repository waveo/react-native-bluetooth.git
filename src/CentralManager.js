import { NativeModules, DeviceEventEmitter } from 'react-native';

export default class CentralManager {

  constructor() {

    this.NativeModule = NativeModules.CentralManagerModule;

    DeviceEventEmitter.addListener("centralManagerDidUpdateState", this.onCentralManagerDidUpdateState);
    DeviceEventEmitter.addListener("centralManagerWillRestoreState", this.onCentralManagerWillRestoreState);
    DeviceEventEmitter.addListener("didDiscoverPeripheral", this.onDidDiscoverPeripheral);
    DeviceEventEmitter.addListener("didConnectPeripheral", this.onDidConnectPeripheral);
    DeviceEventEmitter.addListener("didDisconnectPeripheral", this.onDidDisconnectPeripheral);
    DeviceEventEmitter.addListener("didFailToConnectPeripheral", this.onDidFailToConnectPeripheral);
  }

  // Events Handler

  onCentralManagerDidUpdateState = (event) => {

  }

  onCentralManagerWillRestoreState = (event) => {

  }

  onDidDiscoverPeripheral = (event) => {
    
  }

  onDidConnectPeripheral = (event) => {
    
  }

  onDidDisconnectPeripheral = (event) => {
    
  }

  onDidFailToConnectPeripheral = (event) => {
    
  }

  // Public API

  isScanning () {

  }

  startScan () {
    NativeModule.startScan({
      serviceUUIDs: [Constants.PeripheralServiceUUID]
    });
  }

  startScan () {
    NativeModule.stopScan();
  }

  connectPeripheral() {
    NativeModule.connectPeripheral();
  }

  cancelPeripheralConnection() {
    NativeModule.disconnectPeripheral();
  }

}