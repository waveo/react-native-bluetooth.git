
import CentralManager from './CentralManager'
import PeripheralManager from './PeripheralManager'
import Peripheral from './Peripheral'
import Service from './Service'
import Characteristic from './Characteristic'
import Descriptor from './Descriptor'

export default class BLE {

  static createCentralManager () {
    return new CentralManager();
  }

  static createPeripheralManager() {
    return new PeripheralManager();
  }

  static createService(props) {
    return new Service(props);
  }

  static createCharacteristic(props) {
    return new Characteristic(props);
  }
}

export {
  CentralManager, PeripheralManager, Peripheral, Service, Characteristic, Descriptor
}