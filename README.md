# bluetooth

## Getting started

`$ npm install bluetooth --save`

### Mostly automatic installation

`$ react-native link bluetooth`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `bluetooth` and add `Bluetooth.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libBluetooth.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.emanon.BluetoothPackage;` to the imports at the top of the file
  - Add `new BluetoothPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':bluetooth'
  	project(':bluetooth').projectDir = new File(rootProject.projectDir, 	'../node_modules/bluetooth/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':bluetooth')
  	```


## Usage
```javascript
import Bluetooth from 'bluetooth';

// TODO: What to do with the module?
Bluetooth;
```
