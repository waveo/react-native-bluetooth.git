#!/bin/sh
# 2019-10-31
# mingchen@waveo.com
# 
# clean temporary generated files scripts
#

rm -rf node_modules 
rm -rf ios/Pods
rm -rf android/app/build
rm -rf android/build
rm -rf android/.idea

yarn
cd ios
pod install
cd .. 
