import React from 'react';
import {
  View,
  NativeEventEmitter,
  StyleSheet,
  FlatList,
  Text,
} from 'react-native';

import {CentralManagerModule} from '@emanon_/react-native-bluetooth';
import {Navigation} from 'react-native-navigation';
import Constants from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default class CentralScan extends React.Component {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);

    this.state = {
      peripherals: [{key: 'a'}, {key: 'b'}, {key: 'c'}],
    };
  }

  componentDidMount() {
    this.CentralManagerEmitter = new NativeEventEmitter(CentralManagerModule);

    this.CentralManagerEmitter.addListener(
      'centralManagerDidUpdateState',
      this.onCentralManagerDidUpdateState,
    );

    this.CentralManagerEmitter.addListener(
      'didDiscoverPeripheral',
      this.onDidDiscoverPeripheral,
    );

    this.CentralManagerEmitter.addListener(
      'didConnectPeripheral',
      this.onDidConnectPeripheral,
    );

    this.CentralManagerEmitter.addListener(
      'didDisconnectPeripheral',
      this.onDidDisconnectPeripheral,
    );

    this.CentralManagerEmitter.addListener(
      'didFailToConnectPeripheral',
      this.onDidFailToConnectPeripheral,
    );
  }

  navigationButtonPressed({buttonId}) {
    switch (buttonId) {
      case 'scan':
        CentralManagerModule.startScan({
          serviceUUIDs: [Constants.PeripheralServiceUUID],
        });
        break;
      default:
        break;
    }
  }

  onCentralManagerDidUpdateState = event => {
    console.log('onCentralManagerDidUpdateState', event);
  };

  onDidDiscoverPeripheral = event => {
    console.log('didDiscoverPeripheral', event);
  };

  onDidConnectPeripheral = event => {
    console.log('didConnectPeripheral', event);
  };

  onDidDisconnectPeripheral = event => {
    console.log('didDisconnectPeripheral', event);
  };

  onDidFailToConnectPeripheral = event => {
    console.log('didFailToConnectPeripheral', event);
  };

  renderPeripheralsItem = ({item}) => <Text>{item.key}</Text>;

  render() {
    const {peripherals} = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          data={peripherals}
          extraData={this.state}
          renderItem={this.renderPeripheralsItem}
        />
      </View>
    );
  }
}
