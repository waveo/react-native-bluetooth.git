/**
 * @format
 */

import App from './AppBluetooth';
import CentralScan from './src/container/centralscan';
import CentralInteractive from './src/container/centralinteractive';
import PeripheralAdvertisement from './src/container/peripheraladvertisement';
import PeripheralInteractive from './src/container/peripheralinteractive';
import {Navigation} from 'react-native-navigation';

// AppRegistry.registerComponent(appName, () => App);

Navigation.registerComponent('mix', () => App);
Navigation.registerComponent('scan', () => CentralScan);
Navigation.registerComponent('interactive1', () => CentralInteractive);
Navigation.registerComponent('advertimsement', () => PeripheralAdvertisement);
Navigation.registerComponent('interactive2', () => PeripheralInteractive);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'scan',
                    passProps: {},
                    options: {
                      topBar: {
                        title: {
                          text: 'Scan',
                        },
                        rightButtons: [
                          {
                            id: 'scan',
                            text: 'Start',
                          },
                        ],
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTab: {
                  text: 'Scan',
                  testID: 'Scan',
                },
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'interactive1',
                    passProps: {},
                    options: {
                      topBar: {
                        title: {
                          text: 'Interactive',
                        },
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTab: {
                  text: 'Interactive',
                  testID: 'Interactive',
                },
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'advertimsement',
                    passProps: {},
                    options: {
                      topBar: {
                        title: {
                          text: 'Advertisement',
                        },
                        rightButtons: [
                          {
                            id: 'scan',
                            text: 'Start',
                          },
                        ],
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTab: {
                  text: 'Advertisement',
                  testID: 'Advertisement',
                },
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'interactive2',
                    passProps: {},
                    options: {
                      topBar: {
                        title: {
                          text: 'Interactive',
                        },
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTab: {
                  text: 'Interactive',
                  testID: 'Interactive',
                },
              },
            },
          },
        ],
      },
    },
  });
});
