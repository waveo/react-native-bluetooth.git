import { NativeModules } from 'react-native';

const { Bluetooth, CentralManagerModule, PeripheralManagerModule } = NativeModules;

export default Bluetooth;

export { Bluetooth, CentralManagerModule, PeripheralManagerModule }

