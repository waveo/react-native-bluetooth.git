//
//  BluetoothUtils.h
//  bluetooth
//
//  Created by mingchen on 9/30/19.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

@interface BluetoothUtils : NSObject

+ (NSDictionary*) service2Data:(CBService*)service;

+ (NSArray*) services2Data:(NSArray<CBService*>*)services;

+ (NSDictionary*) peripheral2Data:(CBPeripheral*)peripheral;

+ (NSArray*) peripheralss2Data:(NSArray<CBPeripheral*>*)peripherals;

+ (NSDictionary*) characteristic2Data:(CBCharacteristic*)characteristic;

+ (NSArray*) characteristics2Data:(NSArray<CBCharacteristic*>*)characteristics;

+ (NSDictionary*) central2Data:(CBCentral*)central;

+ (NSMutableArray<CBUUID *> *_Nullable)CBUUIDArrayFromStringArray:(NSArray<id> *)array;

@end

NS_ASSUME_NONNULL_END
