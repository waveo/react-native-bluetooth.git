//
//  BluetoothUtils.m
//  bluetooth
//
//  Created by mingchen on 9/30/19.
//

#import "BluetoothUtils.h"

@implementation BluetoothUtils

+ (NSDictionary*) service2Data:(CBService*)service
{
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    
    data[@"uuid"] = service.UUID.UUIDString;
    data[@"primary"] = [[NSNumber alloc] initWithBool:service.isPrimary];
    data[@"characteristics"] = [BluetoothUtils characteristics2Data:service.characteristics];
    
    return data;
}

+ (NSArray*) services2Data:(NSArray<CBService*>*)services
{
    NSMutableArray<NSDictionary*>* data = [NSMutableArray array];
    
    for (CBService* service in services) {
        [data addObject:[BluetoothUtils service2Data:service]];
    }
    
    return data;
}

+ (NSDictionary*) peripheral2Data:(CBPeripheral*)peripheral
{
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    
    data[@"identifier"] = peripheral.identifier.UUIDString;
    data[@"name"] = peripheral.name;
    data[@"state"] = @(peripheral.state);
    data[@"services"] = [BluetoothUtils services2Data:peripheral.services];
    
    return data;
}

+ (NSArray*) peripherals2Data:(NSArray<CBPeripheral*>*)peripherals
{
    NSMutableArray<NSDictionary*>* data = [NSMutableArray array                                         ];
    
    for (CBPeripheral* peripheral in peripherals) {
        [data addObject:[BluetoothUtils peripheral2Data:peripheral]];
    }
    
    return data;
}

+ (NSDictionary*) characteristic2Data:(CBCharacteristic*)characteristic
{
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    
    data[@"uuid"] = characteristic.UUID.UUIDString;
    data[@"notify"] = [[NSNumber alloc] initWithBool:characteristic.isNotifying];
    data[@"properties"] = [[NSNumber alloc] initWithBool:characteristic.properties];
    data[@"value"] = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
    
    return data;
}

+ (NSArray*) characteristics2Data:(NSArray<CBCharacteristic*>*)characteristics
{
    NSMutableArray<NSDictionary*>* data = [NSMutableArray array];
    
    for (CBCharacteristic* characteristic in characteristics) {
        [data addObject:[BluetoothUtils characteristic2Data:characteristic]];
    }
    
    return data;
}

+ (NSDictionary*) central2Data:(CBCentral*)central
{
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    
    data[@"uuid"] = central.identifier.UUIDString;
    data[@"maximumUpdateValueLength"] = @(central.maximumUpdateValueLength);
    
    return data;
}

+ (NSArray*) centrals2Data:(NSArray<CBCentral*>*)centrals
{
    NSMutableArray<NSDictionary*>* data = [NSMutableArray array];
    
    for (CBCentral* central in centrals) {
        [data addObject:[BluetoothUtils central2Data:central]];
    }
    
    return data;
}

+ (NSMutableArray<CBUUID *> *_Nullable)CBUUIDArrayFromStringArray:(NSArray<id> *)array
{
    if (array == nil || array.count == 0) {
        return nil;
    }
    
    NSMutableArray<CBUUID *> *result = [NSMutableArray array];
    
    for (NSString* uuid in array) {
        [result addObject:[CBUUID UUIDWithString:uuid]];
    }
    
    return result;
}

@end
