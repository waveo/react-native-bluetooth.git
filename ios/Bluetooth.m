#import "Bluetooth.h"


@implementation Bluetooth

RCT_EXPORT_MODULE()

- (NSArray<NSString *> *)supportedEvents
{
    return @[];
}

RCT_EXPORT_METHOD(hasBluetoothPermissions:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    BOOL result = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSBluetoothPeripheralUsageDescription"] != nil;
    resolve(@(result));
}

@end
