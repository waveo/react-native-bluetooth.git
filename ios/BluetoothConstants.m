//
//  Constants.m
//  bluetooth
//
//  Created by mingchen on 10/6/19.
//

#import "BluetoothConstants.h"

@implementation CentralManagerEvent

+ (NSString *)didUpdateState
{
    return @"centralManagerDidUpdateState";
}
+ (NSString *)willRestoreState
{
    return @"centralManagerWillRestoreState";
}
+ (NSString *)didDiscoverPeripheral
{
    return @"didDiscoverPeripheral";
}
+ (NSString *)didConnectPeripheral
{
    return @"didConnectPeripheral";
}
+ (NSString *)didDisconnectPeripheral
{
    return @"didDisconnectPeripheral";
}
+ (NSString *)didFailToConnectPeripheral
{
    return @"didFailToConnectPeripheral";
}

@end

@implementation PeripheralEvent

+ (NSString *)didUpdateName
{
    return @"didUpdateName";
}
+ (NSString *)didModifyServices
{
    return @"didModifyServices";
}
+ (NSString *)didUpdateRSSI
{
    return @"didUpdateRSSI";
}
+ (NSString *)didReadRSSI
{
    return @"didReadRSSI";
}
+ (NSString *)didDiscoverServices
{
    return @"didDiscoverServices";
}
+ (NSString *)didDiscoverIncludedServicesForService
{
    return @"didDiscoverIncludedServicesForService";
}
+ (NSString *)didDiscoverCharacteristicsForService
{
    return @"didDiscoverCharacteristicsForService";
}
+ (NSString *)didUpdateValueForCharacteristic
{
    return @"didUpdateValueForCharacteristic";
}
+ (NSString *)didWriteValueForCharacteristic
{
    return @"didWriteValueForCharacteristic";
}
+ (NSString *)didUpdateNotificationStateForCharacteristic
{
    return @"didUpdateNotificationStateForCharacteristic";
}
+ (NSString *)didDiscoverDescriptorsForCharacteristic
{
    return @"didDiscoverDescriptorsForCharacteristic";
}
+ (NSString *)didUpdateValueForDescriptor
{
    return @"didUpdateValueForDescriptor";
}
+ (NSString *)didWriteValueForDescriptor
{
    return @"didWriteValueForDescriptor";
}
+ (NSString *)isReadyToSendWriteWithoutResponse
{
    return @"isReadyToSendWriteWithoutResponse";
}
+ (NSString *)didOpenL2CAPChannel
{
    return @"peripheralDidOpenL2CAPChannel";
}

@end

@implementation PeripheralManagerEvent

+ (NSString *)didUpdateState
{
    return @"peripheralManagerDidUpdateState";
}
+ (NSString *)willRestoreState
{
    return @"peripheralManagerWillRestoreState";
}
+ (NSString *)didAddService
{
    return @"didAddService";
}
+ (NSString *)didStartAdvertising
{
    return @"didStartAdvertising";
}
+ (NSString *)didCentralConnected
{
    return @"didCentralConnected";
}
+ (NSString *)didCentralDisconnected
{
    return @"didCentralDisconnected";
}
+ (NSString *)didSubscribeToCharacteristic
{
    return @"didSubscribeToCharacteristic";
}

+ (NSString *)didUnsubscribeFromCharacteristic
{
    return @"didUnsubscribeFromCharacteristic";
}

+ (NSString *)didReceiveReadRequest
{
    return @"didReceiveReadRequest";
}

+ (NSString *)didReceiveWriteRequests
{
    return @"didReceiveWriteRequests";
}

+ (NSString *)isReadyToUpdateSubscribers
{
    return @"isReadyToUpdateSubscribers";
}

+ (NSString *)didPublishL2CAPChannel
{
    return @"didPublishL2CAPChannel";
}

+ (NSString *)didUnpublishL2CAPChannel
{
    return @"didUnpublishL2CAPChannel";
}

+ (NSString *)didOpenL2CAPChannel
{
    return @"peripheralManagerDidOpenL2CAPChannel";
}

@end

@implementation BluetoothConstants

+ (NSString *)CCCD { return @"00002902-0000-1000-8000-00805f9b34fb"; }

@end
