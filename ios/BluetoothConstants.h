//
//  Constants.h
//  bluetooth
//
//  Created by mingchen on 10/6/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CentralManagerEvent : NSObject

@property (class, readonly, nonatomic, strong) NSString* didUpdateState;
@property (class, readonly, nonatomic, strong) NSString* willRestoreState;
@property (class, readonly, nonatomic, strong) NSString* didDiscoverPeripheral;
@property (class, readonly, nonatomic, strong) NSString* didConnectPeripheral;
@property (class, readonly, nonatomic, strong) NSString* didDisconnectPeripheral;
@property (class, readonly, nonatomic, strong) NSString* didFailToConnectPeripheral;

@end

@interface PeripheralEvent : NSObject

@property (class, readonly, nonatomic, strong) NSString* didUpdateName;
@property (class, readonly, nonatomic, strong) NSString* didModifyServices;
@property (class, readonly, nonatomic, strong) NSString* didUpdateRSSI;
@property (class, readonly, nonatomic, strong) NSString* didReadRSSI;
@property (class, readonly, nonatomic, strong) NSString* didDiscoverServices;
@property (class, readonly, nonatomic, strong) NSString* didDiscoverIncludedServicesForService;
@property (class, readonly, nonatomic, strong) NSString* didDiscoverCharacteristicsForService;
@property (class, readonly, nonatomic, strong) NSString* didUpdateValueForCharacteristic;
@property (class, readonly, nonatomic, strong) NSString* didWriteValueForCharacteristic;
@property (class, readonly, nonatomic, strong) NSString* didUpdateNotificationStateForCharacteristic;
@property (class, readonly, nonatomic, strong) NSString* didDiscoverDescriptorsForCharacteristic;
@property (class, readonly, nonatomic, strong) NSString* didUpdateValueForDescriptor;
@property (class, readonly, nonatomic, strong) NSString* didWriteValueForDescriptor;
@property (class, readonly, nonatomic, strong) NSString* isReadyToSendWriteWithoutResponse;
@property (class, readonly, nonatomic, strong) NSString* didOpenL2CAPChannel;

@end

@interface PeripheralManagerEvent : NSObject

@property (class, readonly, nonatomic, strong) NSString* didUpdateState;
@property (class, readonly, nonatomic, strong) NSString* willRestoreState;
@property (class, readonly, nonatomic, strong) NSString* didAddService;
@property (class, readonly, nonatomic, strong) NSString* didStartAdvertising;
@property (class, readonly, nonatomic, strong) NSString* didCentralConnected;
@property (class, readonly, nonatomic, strong) NSString* didCentralDisconnected;
@property (class, readonly, nonatomic, strong) NSString* didSubscribeToCharacteristic;
@property (class, readonly, nonatomic, strong) NSString* didUnsubscribeFromCharacteristic;
@property (class, readonly, nonatomic, strong) NSString* didReceiveReadRequest;
@property (class, readonly, nonatomic, strong) NSString* didReceiveWriteRequests;
@property (class, readonly, nonatomic, strong) NSString* isReadyToUpdateSubscribers;
@property (class, readonly, nonatomic, strong) NSString* didPublishL2CAPChannel;
@property (class, readonly, nonatomic, strong) NSString* didUnpublishL2CAPChannel;
@property (class, readonly, nonatomic, strong) NSString* didOpenL2CAPChannel;

@end

@interface BluetoothConstants : NSObject

@property (class, readonly, nonatomic, strong) NSString* CCCD;

@end

NS_ASSUME_NONNULL_END
