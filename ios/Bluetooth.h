#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface Bluetooth : RCTEventEmitter <RCTBridgeModule>

@end
