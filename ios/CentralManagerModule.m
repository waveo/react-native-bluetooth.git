//
//  CertralManagerModule.m
//  bluetooth
//
//  Created by mingchen on 9/30/19.
//

#import "CentralManagerModule.h"

#import <CoreBluetooth/CoreBluetooth.h>
#import "BluetoothUtils.h"
#import "BluetoothConstants.h"

@interface CentralManagerModule () <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (strong, nonatomic, nonnull) CBCentralManager* centralManager;

@property (strong, nonatomic, nonnull) NSMutableArray<CBPeripheral*>* discoveredPeripherals;

@property (strong, nonatomic, nonnull) NSMutableArray<CBPeripheral*>* connectedPeripherals;

@end

@implementation CentralManagerModule

RCT_EXPORT_MODULE()

- (instancetype)init
{
    self = [super init];
    if (self) {
        _discoveredPeripherals = [NSMutableArray array];
        _connectedPeripherals = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Properties

#pragma makr - Private API

- (CBPeripheral*)getDiscoveredPeripheral:(NSDictionary*)peripheralInfo
{
    for (CBPeripheral* peripheral in self.discoveredPeripherals) {
        if ([peripheral.identifier.UUIDString isEqualToString:peripheralInfo[@"identifier"]]) {
            return peripheral;
        }
    }
    return nil;
}

- (CBPeripheral*)getConnectedPeripheral:(NSDictionary*)peripheralInfo
{
    for (CBPeripheral* peripheral in self.connectedPeripherals) {
        if ([peripheral.identifier.UUIDString isEqualToString:peripheralInfo[@"identifier"]]) {
            return peripheral;
        }
    }
    return nil;
}

- (CBService*)getService:(CBPeripheral*)peripheral serviceInfo:(NSDictionary*)serviceInfo
{
    for (CBService* service in peripheral.services) {
        if ([service.UUID.UUIDString isEqualToString:serviceInfo[@"uuid"]]) {
            return service;
        }
    }
    return nil;
}

- (CBCharacteristic*)getCharacteristic:(CBService*)service characteristicInfo:(NSDictionary*)characteristicInfo
{
    for (CBCharacteristic* characteristic in service.characteristics) {
        if ([characteristic.UUID.UUIDString isEqualToString:characteristicInfo[@"uuid"]]) {
            return characteristic;
        }
    }
    return nil;
}

- (CBDescriptor*)getDescriptor:(CBCharacteristic*)characteristic descriptorInfo:(NSDictionary*)descriptorInfo
{
    for (CBDescriptor* descriptor in characteristic.descriptors) {
        if ([descriptor.UUID.UUIDString isEqualToString:descriptorInfo[@"uuid"]]) {
            return descriptor;
        }
    }
    return nil;
}

#pragma mark - Publc Evnets

- (NSArray<NSString *> *)supportedEvents
{
    return @[
             CentralManagerEvent.didUpdateState,
             CentralManagerEvent.willRestoreState,
             CentralManagerEvent.didDiscoverPeripheral,
             CentralManagerEvent.didConnectPeripheral,
             CentralManagerEvent.didDisconnectPeripheral,
             CentralManagerEvent.didFailToConnectPeripheral,
             
             PeripheralEvent.didUpdateName,
             PeripheralEvent.didModifyServices,
             PeripheralEvent.didUpdateRSSI,
             PeripheralEvent.didReadRSSI,
             PeripheralEvent.didDiscoverServices,
             PeripheralEvent.didDiscoverIncludedServicesForService,
             PeripheralEvent.didDiscoverCharacteristicsForService,
             PeripheralEvent.didUpdateValueForCharacteristic,
             PeripheralEvent.didWriteValueForCharacteristic,
             PeripheralEvent.didUpdateNotificationStateForCharacteristic,
             PeripheralEvent.didDiscoverDescriptorsForCharacteristic,
             PeripheralEvent.didUpdateValueForDescriptor,
             PeripheralEvent.didWriteValueForDescriptor,
             PeripheralEvent.isReadyToSendWriteWithoutResponse,
             PeripheralEvent.didOpenL2CAPChannel,
             ];
}

- (NSDictionary *)constantsToExport
{
    return @{};
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

#pragma mark - Public API of CBCentralManager

RCT_EXPORT_METHOD(createCentralManager)
{
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue() options:nil];
}

RCT_EXPORT_METHOD(startScan:(NSDictionary*)options)
{
    if (![self.centralManager isScanning])
    {
        NSMutableArray<CBUUID *> *uuids = [BluetoothUtils CBUUIDArrayFromStringArray:[options objectForKey:@"serviceUUIDs"]];
        NSMutableDictionary *filters = [NSMutableDictionary dictionary];
        
        if ([options objectForKey:@"allowDuplicates"] != nil) {
            [filters setObject:[options objectForKey:@"allowDuplicates"]
                        forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
        }
        
        if ([options objectForKey:@"serviceUUIDs"] != nil) {
            [filters setObject:uuids
                        forKey:CBCentralManagerScanOptionSolicitedServiceUUIDsKey];
        }
        
        [self.centralManager scanForPeripheralsWithServices:uuids options:nil];
    }
}

RCT_EXPORT_METHOD(stopScan)
{
    if ([self.centralManager isScanning])
    {
        [self.centralManager stopScan];
    }
}

RCT_EXPORT_METHOD(connectPeripheral:(NSDictionary*)peripheralInfo)
{
    [self.centralManager connectPeripheral:[self getDiscoveredPeripheral:peripheralInfo] options:nil];
}

RCT_EXPORT_METHOD(disconnectPeripheral:(NSDictionary*)peripheralInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    [self.centralManager cancelPeripheralConnection:peripheral];
    [self.connectedPeripherals removeObject:peripheral];
    peripheral = nil;
}

#pragma mark - Public API of CBPeripheral

RCT_EXPORT_METHOD(discoverServices:(NSDictionary*)peripheralInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    [peripheral discoverServices:nil];
}

RCT_EXPORT_METHOD(discoverCharacteristics:(NSDictionary*)peripheralInfo service:(NSDictionary*)serviceInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    NSAssert(peripheral != nil, @"peripheral is nil");
    
    CBService* service = [self getService:peripheral serviceInfo:serviceInfo];
    NSAssert(service != nil, @"service is nil");
    
    [peripheral discoverCharacteristics:nil forService:service];
}

RCT_EXPORT_METHOD(discoverDescriptorsForCharacteristic:(NSDictionary*)peripheralInfo serviceInfo:(NSDictionary*)serviceInfo characteristic:(NSDictionary*)characteristicInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    NSAssert(peripheral != nil, @"peripheral is nil");
    
    CBService* service = [self getService:peripheral serviceInfo:serviceInfo];
    NSAssert(service != nil, @"service is nil");
    
    CBCharacteristic* characteristic = [self getCharacteristic:service characteristicInfo:characteristicInfo];
    NSAssert(characteristic != nil, @"characteristic is nil");
    
    [peripheral discoverDescriptorsForCharacteristic:characteristic];
}

RCT_EXPORT_METHOD(readCharacteristic:(NSDictionary*)peripheralInfo serviceInfo:(NSDictionary*)serviceInfo characteristic:(NSDictionary*)characteristicInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    NSAssert(peripheral != nil, @"peripheral is nil");
    
    CBService* service = [self getService:peripheral serviceInfo:serviceInfo];
    NSAssert(service != nil, @"service is nil");
    
    CBCharacteristic* characteristic = [self getCharacteristic:service characteristicInfo:characteristicInfo];
    NSAssert(characteristic != nil, @"characteristic is nil");
    
    [peripheral readValueForCharacteristic:characteristic];
}

RCT_EXPORT_METHOD(writeCharacteristic:(NSDictionary*)peripheralInfo serviceInfo:(NSDictionary*)serviceInfo characteristic:(NSDictionary*)characteristicInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    NSAssert(peripheral != nil, @"peripheral is nil");
    
    CBService* service = [self getService:peripheral serviceInfo:serviceInfo];
    NSAssert(service != nil, @"service is nil");
    
    CBCharacteristic* characteristic = [self getCharacteristic:service characteristicInfo:characteristicInfo];
    NSAssert(characteristic != nil, @"characteristic is nil");

    [peripheral writeValue:[characteristicInfo[@"value"] dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}

RCT_EXPORT_METHOD(subscribe:(NSDictionary*)peripheralInfo serviceInfo:(NSDictionary*)serviceInfo characteristic:(NSDictionary*)characteristicInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    NSAssert(peripheral != nil, @"peripheral is nil");
    
    CBService* service = [self getService:peripheral serviceInfo:serviceInfo];
    NSAssert(service != nil, @"service is nil");
    
    CBCharacteristic* characteristic = [self getCharacteristic:service characteristicInfo:characteristicInfo];
    NSAssert(characteristic != nil, @"characteristic is nil");
    
    [peripheral setNotifyValue:YES forCharacteristic:characteristic];
}

RCT_EXPORT_METHOD(unsubscribe:(NSDictionary*)peripheralInfo serviceInfo:(NSDictionary*)serviceInfo  characteristic:(NSDictionary*)characteristicInfo)
{
    CBPeripheral* peripheral = [self getConnectedPeripheral:peripheralInfo];
    NSAssert(peripheral != nil, @"peripheral is nil");
    
    CBService* service = [self getService:peripheral serviceInfo:serviceInfo];
    NSAssert(service != nil, @"service is nil");
    
    CBCharacteristic* characteristic = [self getCharacteristic:service characteristicInfo:characteristicInfo];
    NSAssert(characteristic != nil, @"characteristic is nil");
    
    [peripheral setNotifyValue:NO forCharacteristic:characteristic];
}

#pragma mark - CBPeripheralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@">>>centralManagerDidUpdateState %@ %ld", central, (long)central.state);
    [self sendEventWithName:CentralManagerEvent.didUpdateState body:@{
                                                                      @"state" : @(central.state)
                                                                      }];
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *, id> *)dict
{
    NSLog(@">>>centralManager: willRestoreState %@ %@", central, dict);
    [self sendEventWithName:CentralManagerEvent.willRestoreState body:nil];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSLog(@">>>centralManager: didDiscoverPeripheral %@ %@ %@", peripheral.name, advertisementData, RSSI);
    [self.discoveredPeripherals addObject:peripheral];
    [self sendEventWithName:CentralManagerEvent.didDiscoverPeripheral body:[BluetoothUtils peripheral2Data:peripheral]];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@">>>centralManager: didConnectPeripheral: %@(%@)", peripheral.name, peripheral.identifier.UUIDString);
    
    [peripheral setDelegate:self];
    [self.connectedPeripherals addObject:peripheral];
    [self sendEventWithName:CentralManagerEvent.didConnectPeripheral body:[BluetoothUtils peripheral2Data:peripheral]];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error
{
    NSLog(@">>>centralManager: didFailToConnectPeripheral: %@(%@) - %@", [peripheral name], peripheral.identifier.UUIDString, [error localizedDescription]);
    [self sendEventWithName:CentralManagerEvent.didFailToConnectPeripheral body:[BluetoothUtils peripheral2Data:peripheral]];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error
{
    NSLog(@">>>centralManager: didDisconnectPeripheral: %@(%@) - %@\n", [peripheral name], peripheral.identifier.UUIDString, [error localizedDescription]);
    [self sendEventWithName:CentralManagerEvent.didDisconnectPeripheral body:[BluetoothUtils peripheral2Data:peripheral]];
}

#pragma mark - CBPeripheralDelegate

- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral;
{
    NSLog(@">>>peripheralDidUpdateName");
    [self sendEventWithName:PeripheralEvent.didUpdateName body:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices
{
    NSLog(@">>>peripheral: didModifyServices");
    [self sendEventWithName:PeripheralEvent.didModifyServices body:nil];
}

//- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(nullable NSError *)error
//{
//    NSLog(@">>>peripheralDidUpdateRSSI");
//    [self sendEventWithName:PeripheralEvent.didUpdateRSSI body:nil];
//}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didReadRSSI");
    [self sendEventWithName:PeripheralEvent.didReadRSSI body:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didDiscoverServices：%@", peripheral.services);
    [self sendEventWithName:PeripheralEvent.didDiscoverServices body:[BluetoothUtils services2Data:peripheral.services]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didDiscoverIncludedServicesForService");
    [self sendEventWithName:PeripheralEvent.didDiscoverIncludedServicesForService body:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didDiscoverCharacteristicsForService");
    [self sendEventWithName:PeripheralEvent.didDiscoverCharacteristicsForService body:[BluetoothUtils characteristics2Data:service.characteristics]];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didUpdateValueForCharacteristic");
    [self sendEventWithName:PeripheralEvent.didUpdateValueForCharacteristic body:[BluetoothUtils characteristic2Data:characteristic]];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didWriteValueForCharacteristic");
    [self sendEventWithName:PeripheralEvent.didWriteValueForCharacteristic body:[BluetoothUtils characteristic2Data:characteristic]];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didUpdateNotificationStateForCharacteristic");
    [self sendEventWithName:PeripheralEvent.didUpdateNotificationStateForCharacteristic body:[BluetoothUtils characteristic2Data:characteristic]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didDiscoverDescriptorsForCharacteristic");
    [self sendEventWithName:PeripheralEvent.didDiscoverDescriptorsForCharacteristic body:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didUpdateValueForDescriptor");
    [self sendEventWithName:PeripheralEvent.didUpdateValueForDescriptor body:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didWriteValueForDescriptor");
    [self sendEventWithName:PeripheralEvent.didWriteValueForDescriptor body:nil];
}

- (void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral
{
    NSLog(@">>>peripheralIsReadyToSendWriteWithoutResponse");
    [self sendEventWithName:PeripheralEvent.isReadyToSendWriteWithoutResponse body:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didOpenL2CAPChannel:(nullable CBL2CAPChannel *)channel error:(nullable NSError *)error
{
    NSLog(@">>>peripheral: didOpenL2CAPChannel");
    [self sendEventWithName:PeripheralEvent.didOpenL2CAPChannel body:nil];
}

- (void)invalidate
{
    NSLog(@">>>CertralManagerModule: invalidate");
    // disconnect bluetooth here
}

@end
