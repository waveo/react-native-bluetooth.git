//
//  PeripheralManagerModule.h
//  bluetooth
//
//  Created by mingchen on 9/27/19.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

NS_ASSUME_NONNULL_BEGIN

@interface PeripheralManagerModule : RCTEventEmitter <RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
