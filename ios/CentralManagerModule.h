//
//  CertralManagerModule.h
//  bluetooth
//
//  Created by mingchen on 9/30/19.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

NS_ASSUME_NONNULL_BEGIN

@interface CentralManagerModule : RCTEventEmitter <RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
