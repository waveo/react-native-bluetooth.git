//
//  PeripheralManagerModule.m
//  bluetooth
//
//  Created by mingchen on 9/27/19.
//

#import "PeripheralManagerModule.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "BluetoothUtils.h"
#import "BluetoothConstants.h"

@interface PeripheralManagerModule () <CBPeripheralManagerDelegate>

// 外设相关成员
@property (strong, nonatomic, nonnull) CBPeripheralManager* peripheralManager;
@property (strong, nonatomic, nonnull) NSMutableArray<CBMutableService*>* services;
@property (strong, nonatomic, nonnull) NSMutableArray<CBMutableCharacteristic*>* characteristics;
@property (strong, nonatomic, nonnull) NSMutableArray<CBMutableDescriptor*>* descriptors;
@property (strong, nonatomic, nonnull) NSMutableArray<CBCentral*>* centrals;

@end

@implementation PeripheralManagerModule

- (instancetype)init
{
    self = [super init];
    if (self) {
        _services = [NSMutableArray array];
        _characteristics = [NSMutableArray array];
        _descriptors = [NSMutableArray array];
        _centrals = [NSMutableArray array];
    }
    return self;
}

RCT_EXPORT_MODULE()

#pragma mark - Private API

- (CBMutableService*)getService:(NSDictionary*)serviceInfo
{
    for (CBMutableService* service in self.services) {
        if ([service.UUID.UUIDString isEqualToString:serviceInfo[@"uuid"]]) {
            return service;
        }
    }
    return nil;
}

- (CBMutableCharacteristic*)getCharacteristic:(NSDictionary*)characteristicInfo
{
    for (CBMutableCharacteristic* characteristic in self.characteristics) {
        if ([characteristic.UUID.UUIDString isEqualToString:characteristicInfo[@"uuid"]]) {
            return characteristic;
        }
    }
    return nil;
}

- (CBCentral*)getCentral:(NSDictionary*)centralInfo
{
    for (CBCentral* central in self.centrals) {
        if ([central.identifier.UUIDString isEqualToString:centralInfo[@"identifier"]]) {
            return central;
        }
    }
    return nil;
}

#pragma mark - Public API

RCT_EXPORT_METHOD(createPeripheralManager)
{
    _peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue() options:nil];
}

RCT_EXPORT_METHOD(createDescriptor:(NSDictionary*)descriptorInfo resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
    resolve(nil);
}

RCT_EXPORT_METHOD(createCharacteristic:(NSDictionary*)characteristicInfo resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
    CBMutableCharacteristic* characteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:characteristicInfo[@"uuid"]]
                                                                                 properties:[characteristicInfo[@"properties"] intValue]
                                                                                      value:nil
                                                                                permissions:[characteristicInfo[@"permissions"] intValue]];
    
    [self.characteristics addObject:characteristic];
    resolve([BluetoothUtils characteristic2Data:characteristic]);
}

RCT_EXPORT_METHOD(createService:(NSDictionary*)serviceInfo resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
    CBMutableService* service = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:serviceInfo[@"uuid"]] primary:[serviceInfo[@"primary"] boolValue]];

    NSMutableArray<CBMutableCharacteristic*>* characteristics = [NSMutableArray array];
    for (NSDictionary* characteristicInfo in serviceInfo[@"characteristics"]) {
        [characteristics addObject:[self getCharacteristic:characteristicInfo]];
    }
    [service setCharacteristics:characteristics];
    [self.services addObject:service];
    resolve([BluetoothUtils service2Data:service]);
}

RCT_EXPORT_METHOD(addService:(NSDictionary*)serviceInfo)
{
    CBMutableService* service = [self getService:serviceInfo];
    [self.peripheralManager addService:service];
}

RCT_EXPORT_METHOD(startAdvertise:(NSDictionary*)advertiseData)
{
    NSMutableDictionary *advertisementData = [NSMutableDictionary dictionary];
    [advertisementData setObject:advertiseData[@"localName"] forKey:CBAdvertisementDataLocalNameKey];
    [advertisementData setObject:[BluetoothUtils CBUUIDArrayFromStringArray:advertiseData[@"serviceUUIDs"]] forKey:CBAdvertisementDataServiceUUIDsKey];
    [self.peripheralManager startAdvertising:advertisementData];
}

RCT_EXPORT_METHOD(stopAdvertise)
{
    [self.peripheralManager stopAdvertising];
}

RCT_EXPORT_METHOD(updateValueForCharacteristicOnSubscribedCentrals:(NSDictionary*)characteristicInfo centrals:(NSArray*)centralInfos)
{
    CBMutableCharacteristic* characteristic = [self getCharacteristic:characteristicInfo];
    [self.peripheralManager updateValue:[characteristicInfo[@"value"] dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:characteristic onSubscribedCentrals:nil];
}

#pragma mark - Publc Evnets

- (NSArray<NSString *> *)supportedEvents
{
    return @[
             PeripheralManagerEvent.didUpdateState,
             PeripheralManagerEvent.willRestoreState,
             PeripheralManagerEvent.didAddService,
             PeripheralManagerEvent.didStartAdvertising,
             PeripheralManagerEvent.didCentralConnected,
             PeripheralManagerEvent.didCentralDisconnected,
             PeripheralManagerEvent.didSubscribeToCharacteristic,
             PeripheralManagerEvent.didUnsubscribeFromCharacteristic,
             PeripheralManagerEvent.didReceiveReadRequest,
             PeripheralManagerEvent.didReceiveWriteRequests,
             PeripheralManagerEvent.isReadyToUpdateSubscribers,
             PeripheralManagerEvent.didPublishL2CAPChannel,
             PeripheralManagerEvent.didUnpublishL2CAPChannel,
             PeripheralManagerEvent.didOpenL2CAPChannel
             ];
}

- (NSDictionary *)constantsToExport
{
    return @{};
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

#pragma mark - CBPeripheralManagerDelegate

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    NSLog(@">>>peripheralManagerDidUpdateState %@ %ld", peripheral, (long)peripheral.state);
    
    NSMutableDictionary* body = [NSMutableDictionary dictionary];
    [body setObject:[NSNumber numberWithInteger:peripheral.state] forKey:@"state"];
    
    [self sendEventWithName:PeripheralManagerEvent.didUpdateState body:body];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral willRestoreState:(NSDictionary<NSString *, id> *)dict
{
    NSLog(@"peripheralManager: willRestoreState");
    
    NSMutableDictionary* body = [NSMutableDictionary dictionary];
    [body setObject:[NSNumber numberWithInteger:peripheral.state] forKey:@"state"];
    
    [self sendEventWithName:PeripheralManagerEvent.willRestoreState body:body];
}

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(nullable NSError *)error
{
    NSLog(@">>>peripheralManagerDidStartAdvertising %@", error);
    
    NSMutableDictionary* body = [NSMutableDictionary dictionary];
    [body setObject:[NSNumber numberWithBool:(error == nil)] forKey:@"success"];
    [body setObject:(error == nil ? @"" : [error localizedDescription]) forKey:@"error"];
    
    [self sendEventWithName:PeripheralManagerEvent.didStartAdvertising body:body];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(nullable NSError *)error
{
    NSLog(@">>>peripheralManager: didAddService");
    [self sendEventWithName:PeripheralManagerEvent.didAddService body:[BluetoothUtils service2Data:service]];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic
{
    NSLog(@">>>peripheralManager: central: didSubscribeToCharacteristic");
    [self sendEventWithName:PeripheralManagerEvent.didSubscribeToCharacteristic body:@{@"characteristic" : [BluetoothUtils characteristic2Data:characteristic],
                                                                   @"central" : [BluetoothUtils central2Data:central]}];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic
{
    NSLog(@">>>peripheralManager: central: didUnsubscribeFromCharacteristic");
    [self sendEventWithName:PeripheralManagerEvent.didUnsubscribeFromCharacteristic body:@{@"characteristic" : [BluetoothUtils characteristic2Data:characteristic],
                                                                       @"central" : [BluetoothUtils central2Data:central]}];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request
{
    NSLog(@">>>peripheralManager: didReceiveReadRequest %@", request);
    [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
    [self sendEventWithName:PeripheralManagerEvent.didReceiveReadRequest body:nil];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray<CBATTRequest *> *)requests
{
    NSLog(@">>>peripheralManager: didReceiveWriteRequests");
    
    for (CBATTRequest* req in requests) {
        NSLog(@">>>req: %@ %@", req, [[NSString alloc] initWithData:req.value encoding:NSUTF8StringEncoding]);
        [peripheral respondToRequest:req withResult:CBATTErrorSuccess];
        
        CBMutableCharacteristic* characteristic = (CBMutableCharacteristic*)req.characteristic;
        [characteristic setValue:req.value];
        
        [self sendEventWithName:PeripheralManagerEvent.didReceiveWriteRequests body:[BluetoothUtils characteristic2Data:characteristic]];
    }
}

- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral
{
    NSLog(@">>>peripheralManagerIsReadyToUpdateSubscribers");
    [self sendEventWithName:PeripheralManagerEvent.isReadyToUpdateSubscribers body:nil];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didPublishL2CAPChannel:(CBL2CAPPSM)PSM error:(nullable NSError *)error
{
    NSLog(@">>>peripheralManager: didPublishL2CAPChannel");
    [self sendEventWithName:PeripheralManagerEvent.didPublishL2CAPChannel body:nil];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didUnpublishL2CAPChannel:(CBL2CAPPSM)PSM error:(nullable NSError *)error
{
    NSLog(@">>>peripheralManager: didUnpublishL2CAPChannel");
    [self sendEventWithName:PeripheralManagerEvent.didUnpublishL2CAPChannel body:nil];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didOpenL2CAPChannel:(nullable CBL2CAPChannel *)channel error:(nullable NSError *)error
{
    NSLog(@">>>peripheralManager: didOpenL2CAPChannel");
    [self sendEventWithName:PeripheralManagerEvent.didOpenL2CAPChannel body:nil];
}

@end
